"""
 2.1 创建测试用例方法
        2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
        2.1.2 判断method的请求方式：
            如果是get：进行get请求并得到响应
            如果是post：进行post请求并得到响应
"""
import requests


# 2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect

class ConfigHttp:

    def __init__(self, dic):
        # 2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
        self.url = dic["interfaceUrl"]
        self.method = dic["method"]
        self.value = dic["value"]
        self.expect = dic["expect"]
        self.header = dic["header"]

    def run(self):
        # 2.1.2 判断method的请求方式：
        # 如果是get：进行get请求并得到响应
        if self.method.lower() == "get":
            # 进行请求
            res = self.__get()
            return res
        # 如果是post：进行post请求并得到响应
        elif self.method.lower() == "post":
            res = self.__post()
            return res

    def __get(self):

        res = requests.get(url=self.url, params=eval(self.value), headers=eval(self.header))
        return res

    def __post(self):
        res = requests.post(url=self.url, data=eval(self.value), headers=eval(self.header))
        return res


if __name__ == '__main__':
    from common.readData import ReadData

    rd = ReadData()
    test_data = rd.read_excel()
    print(test_data)

    ch = ConfigHttp(test_data[0])
    test1 = ch.run()
    print(test1.text)
