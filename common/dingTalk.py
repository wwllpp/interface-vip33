"""
定义一个类 DingTalk
定义初始化方法
    创建一个请求头并绑定为实例属性
    将机器人的请求地址绑定为实例属性
定义一个对外方法发送普通消息：send_msg:
    定义发送消息的所有参数json格式
    携带参数进行请求，发送消息
"""
import requests


class DingTalk:
    def __init__(self):
        self.header = {'Content-Type': 'application/json;charset=utf-8'}
        self.url = 'https://oapi.dingtalk.com/robot/send?access_token=ded597142fbee3889e19d9b1ecc06ad89b7b91c7bbc55fd30c38ba06f5f46818'

    def send_msg(self, text):
        dic = {
            "msgtype": "text",
            "text": {"content": text},
            "at": {
                "atMobiles": ["+86-13552828809", "+86-18601916518"],
                "isAtaLL": False}
        }

        res = requests.post(url=self.url, json=dic, headers=self.header)
        return res.text

    def send_link(self, text, url):
        dic = {
            "msgtype": "link",
            "link": {
                "text": text,
                "title": "QQ音乐,听你想听",
                "messageUrl": url
            }
        }

        res = requests.post(url=self.url, json=dic, headers=self.header)
        return res.text

if __name__ == '__main__':
    dt = DingTalk()
    # print(dt.send_msg("测试:我是王李鹏"))
    print(dt.send_link("测试：音乐","https://www.4399.com/"))
