# import logging

# 定义一个对外函数log
# def log():
#     # 使用basicConfig配置日志
#     logging.basicConfig(
#         level=logging.DEBUG,
#         encoding="utf-8",
#         filename=r"D:\CeKai\ProJect\interface-vip33\testLog\log.txt",
#         format="日志:%(name)s-级别:%(levelname)s-时间:%(asctime)s-模块:%(module)s.py-第%(lineno)d:%(message)s"
#
#     )
#     # 创建一个日志记录器
#     logger = logging.getLogger("Test")
#     # 返回给调用者使用
#     return logger

import logging, os
from logging.handlers import TimedRotatingFileHandler

# 获取文件路径
log_path = os.path.dirname(os.path.dirname(__file__)) + f"/testLog/"


def log():
    # 创建日志记录器
    logger = logging.getLogger("Test")
    # 配置日志记录器级别
    logger.setLevel(logging.INFO)
    # 配置日志记录器的输出格式
    format1 = logging.Formatter(
        "日志:%(name)s-级别:%(levelname)s-时间:%(asctime)s-模块:%(module)s.py-第%(lineno)d:%(message)s")
    # 创建并添加日志记录器handler-控制台
    sh = logging.StreamHandler()
    sh.setFormatter(format1)  # 选用输出格式
    logger.addHandler(sh)
    # 创建并添加日志记录器handler-文件
    # filename 文件路径拼接文件名称，when：S秒维度日志，D天维度日志
    fh = TimedRotatingFileHandler(filename=log_path + "log.txt", when="S", backupCount=3, encoding="utf-8")
    fh.setFormatter(format1)  # 选用输出格式
    logger.addHandler(fh)
    # 对外提供日志记录器

    return logger


if __name__ == '__main__':
    logger = log()
    logger.debug("我是debug")
    logger.warning("我是警告")
    logger.critical("我是灾难")
