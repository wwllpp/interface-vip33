""""""
"""
定义一个类
1、定义初始化方法：
    1.1 获取所有测试用例并绑定为实例属性

2、定义一个方法：根据当前的用例(字典)id，执行依赖的前置用例并替换依赖字段：header，value
    2.1 获取用于判断是否有依赖的前置用例的依赖字段：rely,caseid,header,value
    2.2 判断是否有前置用例(rely是否是y)，判断caseid是否有值？
        2.2.1 有依赖：根据正则表达式找出请求头/体里面依赖的字段：可以封装成为一个独立的方法进行调用
        2.2.2 执行前置接口得到前置用例的返回结果，在结果内，根据上一步找到的依赖字段获取响应结果里面的实际结果：可以封装成为一个独立的方法进行调用
        2.2.3 上一步获取的依赖的实际结果，替换原本的数据
        2.2.4 返回替换好的header和value
    2.3 否：没有依赖：直接返回当前的header和value
    
3、定义一个方法：根据需求提取出需要依赖的请求头/体里面的依赖字段
    3.1 根据正则表达式提取出依赖字段，得到的是一个列表
    3.2 判断得到的列表是否有数据
        3.2.1 是：返回依赖的字段
        3.2.2 否：返回None
        
4、定义一个方法：执行前置用例，根据准备好的目标字段提取实际数据，响应头/体
    4.1 准备前置用例请求所需要的数据
    4.2 调用configHttp模块 进行接口请求，并得到返回结果
    4.3 判断请求头里面是否有依赖字段，有则返回依赖的数据
    4.4 判断请求体里面是否有依赖字段，有则返回依赖的数据
    4.5 返回给外界得到的依赖数据
"""
import re
from common.configHttp import ConfigHttp
from jsonpath import jsonpath


# 定义一个类
class PreSolve:
    # 1、定义初始化方法：
    def __init__(self, testdata):

        # 1.1 获取所有测试用例并绑定为实例属性
        self.testdata = testdata

    # 2、定义一个方法：根据当前的用例(字典)id，执行依赖的前置用例并替换依赖字段：header，value
    def presolve(self, dic):
        # 2.1 获取用于判断是否有依赖的前置用例的依赖字段：rely,caseid,header,value
        rely, caseid, header, value = dic["rely"], dic["caseid"], dic["header"], dic["value"]
        # 2.2 判断是否有前置用例(rely是否是y)，判断caseid是否有值？
        if rely.lower() == "y" and caseid != "":
            # 2.2.1 有依赖：根据正则表达式找出请求头/体里面依赖的字段：可以封装成为一个独立的方法进行调用
            # 获取用例里面的需要 依赖的字段数据  "${Set-Cookie}" '${username}'
            goal_header = self.get_predata(header)
            goal_body = self.get_predata(value)
            # print(f"\n依赖的头里面的字段：{goal_header},\n依赖的体里面的字段：{goal_body}")
            # 2.2.2 执行前置接口得到前置用例的返回结果，在结果内，根据上一步找到的依赖字段获取响应结果里面的实际结果：可以封装成为一个独立的方法进行调用
            h, b = self.run_pre(caseid, goal_header, goal_body)
            # 2.2.3 上一步获取的依赖的实际结果，替换原本的数据
            if h != None:
                header = header.replace("${" + goal_header + "}", h)
            if b != None:
                value = value.replace("${" + goal_body + "}", b)
            # 2.2.4 返回替换好的header和value
            return header, value
        # 2.3 否：没有依赖：直接返回当前的header和value
        else:
            return header, value

    # 3、定义一个方法：根据需求提取出需要依赖的请求头/体里面的依赖字段
    @staticmethod
    def get_predata(data):
        # 3.1 根据正则表达式提取出依赖字段，得到的是一个列表
        res = re.findall(r"\${(.*?)}", data)
        # 3.2 判断得到的列表是否有数据
        if len(res) != 0:
            # 3.2.1 是：返回依赖的字段
            return res[0]
            # 3.2.2 否：返回None

    # 4、定义一个方法：执行前置用例，根据准备好的目标字段提取实际数据，响应头/体
    def run_pre(self, caseid, goal_header=None, goal_body=None):
        # 4.1 准备前置用例请求所需要的数据
        data = self.testdata[int(caseid) - 1]  # 控制获取excel第一条
        # print(data)
        # 4.2 调用configHttp模块 进行接口请求，并得到返回结果
        ch = ConfigHttp(data)
        res = ch.run()
        # print(res.text)
        # print(f"\n请求头：{res.headers}")
        # print(f"\n请求体：{res.text}")

        # print(res.json())
        # 4.3 判断请求头里面是否有依赖字段，有则返回依赖的数据
        if goal_header != None:
            goal_header = res.headers[goal_header]
        # 4.4 判断请求体里面是否有依赖字段，有则返回依赖的数据
        if goal_body != None:
            goal_body = jsonpath(res.json(), f"$..{goal_body}")[0]
        # 4.5 返回给外界得到的依赖数据
        return goal_header, goal_body


if __name__ == '__main__':
    # 准备测试数据
    from common.readData import ReadData

    rd = ReadData()
    testdata = rd.read_excel()
    # print(testdata[4])
    # 调试当前类
    ps = PreSolve(testdata)
    print(ps.presolve(testdata[4]))

    # 测试数据
    # str1 = "{'name':'${username}','link':'www.baidu.com'}"
    # str2 = '{"cookie":"${Set-Cookie}"}'
    # str3 = "{'username':'liangchao','password':'123456'}"
    # print(ps.get_predata(str1))
    # print(ps.get_predata(str2))
    # print(ps.get_predata(str3))

    # 调试第三个方法
    # print(ps.run_pre("1", goal_header="Set-Cookie", goal_body="username"))
    # print(ps.run_pre("1", goal_header="Server", goal_body="admin"))
