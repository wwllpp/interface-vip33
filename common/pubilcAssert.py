# # 扩展知识点
import jsonpath


#
# data = {"name": "Tom",
#         "msg": {"age": 18,
#                 "name": "汤姆"}
#         }
#
# # 获取字典内第一层里面指定key对应的value
# res = jsonpath.jsonpath(data, "$.name")
# print(res)  # ['Tom']
#
# # 获取字典内每一层里面指定key对应的value
# res1 = jsonpath.jsonpath(data, "$..name")
# print(res1)

#
# """
# 定义一个类
#         初始化方法
#                 获取预期结果
#                 获取实际结果
#         定一个对外的断言方法
#                 断言状态码是否正确
#                 循环断言字典里面的键值对
#                         根据提出的key获取返回结果响应内的实际结果
#                         断言取到的实际结果与预期结果是否相同
# """
# 定义一个类
class PublicAssert:
    # 初始化方法
    def __init__(self, dic, res):
        # 获取预期结果
        self.expect = dic["expect"]
        # 获取实际结果
        self.res = res

        # print(self.expect)
        # print(self.res.json())

    # 定一个对外的断言方法
    def public_assert(self):
        # 断言状态码是否正确
        assert self.res.status_code in [200, 304], f"请求失败，响应的状态码是{self.res.status_code}"

        # 返回值为html格式的断言处理
        if self.res.text.startswith("<!DOCTYPE html>"):
            # 断言预期结果字段是否在html源码中
            assert self.expect in self.res.text, f"断言字段没有在html响应内"

        else:
            # 返回值为json格式的断言处理
            # 提前预设一个保存报错信息的字符串，初始为空
            self.expect = eval(self.expect)
            msg = ""
            # 循环断言字典里面的键值对
            for k, v in self.expect.items():

                # 根据提出的key获取返回结果响应内的实际结果
                real = jsonpath.jsonpath(self.res.json(), f"$..{k}")
                # print(real)
                # print(real[0])
                # 断言取到的实际结果与预期结果是否相同
                if real == False:
                    # raise AssertionError(f"响应体内未找到预取结果字段：{k}")
                    # 将报错信息写道msg里面
                    msg += f"\n响应体内未找到预期结果字段：{k}"
                else:
                    # assert str(real[0]) == str(v), f"预期结果与实际结果不一致：{real[0]}=== {v}"
                    # 如果预期结果与实际结果不符，将报错信息加到msg里面
                    if str(real[0]) != str(v):
                        msg += f"\n预期结果与实际结果不一致：{real[0]} === {v}"

            # 判断msg是否为空串
            assert msg == "", msg



if __name__ == '__main__':
    # 获取测试数据
    from common.readData import ReadData
    from common.configHttp import ConfigHttp

    rd = ReadData()
    test_data = rd.read_excel()
    # print(test_data)

    ch = ConfigHttp(test_data[3])
    res = ch.run()
    # print(res.text)

    # 测试当前类
    pa = PublicAssert(test_data[3], res)
    pa.public_assert()
