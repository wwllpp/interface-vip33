"""
创建一个类
    定义初始化方法
        获取配置文件的路径
        实例化configpraser类
        读取指定路径下的配置文件信息
        获取所有seciton值
    定义一个对外方法，获取某个section下面的所有的option
    定义一个对外方法，获取某个section下面某个option的值
"""

from configparser import ConfigParser
import os


# 创建一个类
class ReadConfig:
    # 定义初始化方法
    def __init__(self):
        # 获取配置文件的路径
        self.path = os.path.dirname(os.path.dirname(__file__)) + r"/config.ini"
        # 实例化configpraser类
        self.conf = ConfigParser()
        # 读取指定路径下的配置文件信息
        self.conf.read(self.path, encoding="utf-8")
        # 获取所有seciton值
        # print(f"所有的section值:{self.conf.sections()}")

    # 定义一个对外方法，获取某个section下面的所有的option
    def get_all_option(self, s):
        return self.conf.items(s)

    # 定义一个对外方法，获取某个section下面某个option的值
    def get_one_option(self, s, o):
        return self.conf.get(s, o)

    # 用不定长参数实现
    # def get_config(self, *args):
    #     if len(args) == 1:
    #         return self.conf.items(args[0])
    #     elif len(args) == 2:
    #         return self.conf.get(*args)

    # 用默认参数实现
    def get_config(self, s, o=''):
        if o == "":
            return self.conf.items(s)
        else:
            return self.conf.get(s, o)


if __name__ == '__main__':
    rc = ReadConfig()
    # print(rc.get_all_option("mysql"))
    # print(rc.get_one_option("redis","host"))
    # print(rc.get_config(user))

    # print(rc.get_config("mysql", "host"))
    # print(rc.get_config("mysql"))
    # print(rc.get_config_1("mysql",'host'))
    print(rc.get_config("mysql"))
