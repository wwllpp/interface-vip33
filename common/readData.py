# 导包


# 打开excel
# readbook = xlrd.open_workbook(r"D:\CeKai\framework_vip33\testData\data.xls")

# path = os.path.dirname(os.path.dirname(__file__)) + "/testData/data.xls"
# print(path)
#
# readbook = xlrd.open_workbook(path)
# # 获取所有sheet页的名字
# print(readbook.sheet_names())
# # 打开指定的sheet页
# sheet = readbook.sheet_by_index(0)
# print(sheet)
# # 获取这个sheet页最大行/列
# max_row = sheet.nrows
# max_col = sheet.ncols
# print(f"最大行{max_row}，最大列{max_col}")
# # 获取某个单元格的值
# print(sheet.cell_value(0, 0))
# # 获取某行的值
# print(sheet.row_values(1))
# print(sheet.col_values(1))

# """
# [[],[],[]]
# [{},{},{}]
# """
# list1 = ["name","age","sex"]
# list2 = ["TOM",18,"男"]
#
# # zip函数实现
# dict2 = dict(zip(list1,list2))
# print(dict2)


"""
定义一个类
1、定义init初始化方法：
    1.1 获取文件路径
    1.2 打开并读取excel
    1.3 获取指定的sheet页
    1.4 获取最大行/列
    1.5 预设一个返回结果列表，默认为空列表
    1.6 获取第一行作为字典的key
2、指定一个组装数据的对外方法：read_excel
    循环读取每一行作为一条测试用例（第一行除外）
    2.1· 获取每一行的数据
    2.2 与第一行组装成为一个字典
    2.3 将组装好的字典放到结果列表中
    2.4 将组装好的结果列表返回给调用者使用
    
3、 定义一个组装数据的对外方法：read_json
    3.1 获取文件路径
    3.2 打开json文件
    3.3 将json文件转化为字典，并存到一个变量内
    3.4 关闭json文件
    3.5 读取变量字典内的所有value，转化成列表
    3.6 返回组装好的列表
4、定义一个组装数据的对外方法：read_yaml
    4.1 获取文件路径
    4.2 打开yaml文件
    4.3 读取yaml文件内容保存到变量内
    4.4 关闭yaml文件
    4.5 返回组装好的数据
"""
import xlrd, os, json
import yaml


# 定义一个类
class ReadData:
    # 1、定义init初始化方法：
    def __init__(self):
        # 1.1 获取文件路径
        self.path_name = os.path.dirname(os.path.dirname(__file__)) + "/testData/data1.xls"
        # 1.2 打开并读取excel
        self.read_book = xlrd.open_workbook(self.path_name)
        # 1.3 获取指定的sheet页
        self.sheet = self.read_book.sheet_by_index(1)
        # 1.4 获取最大行/列
        self.max_row = self.sheet.nrows
        self.max_col = self.sheet.ncols
        # 1.5 预设一个返回结果列表，默认为空列表
        self.res_list = []
        # 1.6 获取第一行作为字典的key
        self.first_row = self.sheet.row_values(0)

    # 2、指定一个组装数据的对外方法：read_excel
    def read_excel(self):
        #     循环读取每一行作为一条测试用例（第一行除外）
        for i in range(1, self.max_row):
            # 2.1· 获取每一行的数据
            row_value = self.sheet.row_values(i)
            # 2.2 与第一行组装成为一个字典
            dict1 = dict(zip(self.first_row, row_value))
            # 2.3 将组装好的字典放到结果列表中
            self.res_list.append(dict1)
        # 2.4 将组装好的结果列表返回给调用者使用
        return self.res_list

    # 3、 定义一个组装数据的对外方法：read_json
    def read_json(self):
        # 3.1 获取文件路径
        json_path = os.path.dirname(os.path.dirname(__file__)) + "/testData/data1.json"
        # 3.2 打开json文件
        f = open(json_path, "r")
        # 3.3 将json文件转化为字典，并存到一个变量内
        testdata = json.load(f)
        # print(testdata)
        # 3.4 关闭json文件
        f.close()
        # 3.5 读取变量字典内的所有value，转化成列表
        testdata1 = list(testdata.values())
        # 3.6 返回组装好的列表
        return testdata1

    # 4、定义一个组装数据的对外方法：read_yaml
    def read_yaml(self):
        #     4.1 获取文件路径
        yaml_path = os.path.dirname(os.path.dirname(__file__)) + "/testData/data1.yaml"
        #     4.2 打开yaml文件
        f = open(yaml_path, "r")
        #     4.3 读取yaml文件内容保存到变量内
        data = yaml.load(f, Loader=yaml.FullLoader)
        #     4.4 关闭yaml文件
        f.close()
        #     4.5 返回组装好的数据
        return data


if __name__ == '__main__':
    rd = ReadData()
    # print(rd.read_excel())
    # print(rd.read_json())
    print(rd.read_yaml())
