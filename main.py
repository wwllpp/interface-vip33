# import pytest
#
# pytest.main(["-vs"])

"""
执行测试用例生成临时测试报告
将临时报告生成为html格式报告

修改测试报告窗口标题：set_window_title
    1、以读取的方式打开index.html文件，并保存到对象内
        1.1 获取文件内所有的内容保存到变量data内
        1.2 关闭文件
    2、以写入的方式打开index.html文件，并保存到对象内
        2.1 遍历data每一行内容，将Allure Report替换为新标题
        2.2 保存关闭\

修改报告内标题：config_title
    获取summary.json的文件内容
    用读取的方式打开文件，并保存到对象f中
    加载json文件内容到遍历data内
    修改内容
    将修改后的内容保存到字典内
    关闭文件
    用写的方式打开并保存到对象w中，修改数据
    将修改后的字典写入json文件内
    关闭文件
发送测试报告
清理测试报告
"""
import time

import pytest, os, json, shutil
from common.sendEmail import SendEmail


def set_window_title(new_name, file_path):
    # 1、以读取的方式打开index.html文件，并保存到对象内
    f = open(f"{file_path}/index.html", "r", encoding="utf-8")
    # 获取文件内所有的内容保存到变量data内
    data = f.readlines()
    # 关闭文件
    f.close()
    # 2、以写入的方式打开index.html文件，并保存到对象内
    w = open(f"{file_path}/index.html", "w", encoding="utf-8")
    # 遍历data每一行内容，将Allure Report替换为新标题
    for line in data:
        w.write(line.replace("Allure Report", new_name))
    # 保存关闭
    w.close()


def config_title(new_name, file_path):
    # 获取summary.json的文件内容
    # 用读取的方式打开文件，并保存到对象f中
    f = open(f"{file_path}/widgets/summary.json", "r", encoding="utf-8")
    # 加载json文件内容到遍历data内
    data = json.load(f)
    # 修改内容
    # 将修改后的内容保存到字典内
    data["reportName"] = new_name
    # 关闭文件
    f.close()
    # 用写的方式打开并保存到对象w中，修改数据
    w = open(f"{file_path}/widgets/summary.json", "w", encoding="utf-8")
    # 将修改后的字典写入json文件内 (ensure_ascii 内部有中文，更换asc码) (indent=4,写入的文件缩进4个空格)
    json.dump(data, w, ensure_ascii=False, indent=4)
    # 关闭文件
    w.close()


# 清理报告
# def auto_clear():
#     # 获取报告路径下所有文件的名字
#     dif = os.path.dirname(__file__) + "/testReport/"
#     # 获取testReport文件下所有的文件名称
#     file_list = os.listdir(dif)
#     print(file_list)
#     # 遍历文件名称进行删除
#     for i in file_list:
#         # 可以把文件和文件夹都删除掉
#         try:
#             # 需要导包 shutil包
#             shutil.rmtree(dif + i)
#         except:
#             os.remove(dif + i)

# 清理报告，保留最新的
def auto_clear(n):
    # 获取报告路径下所有文件的名字
    dif = os.path.dirname(__file__) + "/testReport/"
    # 获取testReport文件下所有的文件名称
    file_list = os.listdir(dif)
    # print(file_list)
    # 根据创建时间将列表重新排序,(匿名函数x，将列表数据挨个给到x，然后获取单个x的创建时间，最后sort进行排序)
    file_list.sort(key=lambda x: os.path.getctime(dif + x))
    # print(file_list)
    # print(file_list[:-n])
    # 遍历文件名称进行删除
    for i in file_list[:-n]:
        # 可以把文件和文件夹都删除掉
        try:
            # 需要导包 shutil包
            shutil.rmtree(dif + i)
        except:
            os.remove(dif + i)


if __name__ == '__main__':
    # 获取当前时间
    cur_time = time.strftime("%Y-%m-%d-%H-%M-%S")
    # 设置报告生成的路径
    path = os.path.dirname(__file__) + "/testReport/temp"
    report = os.path.dirname(__file__) + f"/testReport/{cur_time}"
    # 执行测试用例生成临时测试报告
    pytest.main(["-vs", f"--alluredir={path}", "--clean-alluredir"])
    # 将临时报告转化成真正的html报告
    os.system(f"allure generate {path} -o {report}")

    # 修改报告窗口标题
    set_window_title("vip33期-测开", report)
    # 修改窗口内部标题
    config_title("测试用例", report)

    # 手动创建一个脚本用于查看报告
    f = open(f"{report}/查看报告.bat", "w")
    f.write("allure open ./")
    f.close()

    # 将生成的报告目录压缩成为zip格式
    zip_path = shutil.make_archive(base_name=f"{path}/测试报告",format="zip",root_dir=report)
    print(zip_path)

    # se = SendEmail()
    # se.send(zip_path)

    # 清理报告
    auto_clear(4)
