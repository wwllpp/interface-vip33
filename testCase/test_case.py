"""
导包
1、获取测试数据
2、定义一个测试类
    2.1 创建测试用例方法
        2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
        2.1.2 判断method的请求方式：
            如果是get：进行get请求并得到响应
            如果是post：进行post请求并得到响应
        2.1.3 将放回的json转化为字典
        2.1.4 在字典内获取实际的errorCode的值
        2.1.5 进行断言：判断实际结果与预期结果是否一致
"""

# 导包
import pytest, requests
from common.readData import ReadData
import json
from common.configHttp import ConfigHttp
from common.pubilcAssert import PublicAssert
from common.preSolve import PreSolve

# 1、获取测试数据
re = ReadData()
test_data = re.read_excel()


# print(test_data[0])

# 2、定义一个测试类
class TestCase:
    # 2.1 创建测试用例方法
    # def test_case(self):
    #     # 2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
    #     url = test_data[0]["interfaceUrl"]
    #     method = test_data[0]["method"]
    #     value = test_data[0]["value"]
    #     expect = test_data[0]["expect"]
    #     print(url, method, value, expect)
    #     # 2.1.2 判断method的请求方式：
    #     # 如果是get：进行get请求并得到响应
    #     if method.lower() == "get":
    #         # 进行请求
    #         res = requests.get(url=url,params=value)
    #         # 查看结果
    #         print(res)
    #     # 如果是post：进行post请求并得到响应
    #     elif method.lower() == "post":
    #         res = requests.post(url=url,data=eval(value))
    #         # print(res.text)
    #         # 将字典直接给形参json
    #         # res = requests.post(url=url,json=value)
    #         # print(res.text)
    #         # print(type(res.text))
    #
    #     # 2.1.3 将返回的json转化为字典
    #     res_dict = res.json()
    #     print(res_dict)
    #     print(type(res_dict))
    #     # 2.1.4 在字典内获取实际的errorCode的值
    #     real = res_dict["errorCode"]
    #     print(real)
    #     # 2.1.5 进行断言：判断实际结果与预期结果是否一致
    #     assert str(real) == str(expect),"预期结果与实际结果不否，断言失败"

    # 将参数化parametrize用到用例里
    # @pytest.mark.parametrize("dic", test_data)
    # def test_case(self, dic):
    #     # 2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
    #     url = dic["interfaceUrl"]
    #     method = dic["method"]
    #     value = dic["value"]
    #     expect = dic["expect"]
    #     print(url, method, value, expect)
    #     # 2.1.2 判断method的请求方式：
    #     # 如果是get：进行get请求并得到响应
    #     if method.lower() == "get":
    #         # 进行请求
    #         res = requests.get(url=url, params=value)
    #         # 查看结果
    #         print(res)
    #     # 如果是post：进行post请求并得到响应
    #     elif method.lower() == "post":
    #         res = requests.post(url=url, data=eval(value))
    #         # print(res.text)
    #         # 将字典直接给形参json
    #         # res = requests.post(url=url,json=value)
    #         # print(res.text)
    #         # print(type(res.text))
    #
    #     # 2.1.3 将返回的json转化为字典
    #     res_dict = res.json()
    #     print(res_dict)
    #     print(type(res_dict))
    #     # 2.1.4 在字典内获取实际的errorCode的值
    #     real = res_dict["errorCode"]
    #     print(real)
    #     # 2.1.5 进行断言：判断实际结果与预期结果是否一致
    #     assert str(real) == str(expect), "预期结果与实际结果不否，断言失败"

    # @pytest.mark.parametrize("dic", test_data)
    # def test_case3(self, dic):
    #     # 请求接口获取响应结果
    #     rd = ConfigHttp(dic)
    #     ads = rd.run()
    #     print(ads.text)
    #     # 将返回的json转化为字典
    #     res_dict = ads.json()
    #     print(res_dict)
    #     print(type(res_dict))
    #     # 2.1.4 在字典内获取实际的errorCode的值
    #     real = res_dict["errorCode"]
    #     print(real)
    #     # 2.1.5 进行断言：判断实际结果与预期结果是否一致
    #     assert str(real) == str(dic["expect"]), "预期结果与实际结果不否，断言失败"

    # 断言中的抗
    # 坑一：用if和else进行断言,记得抛出异常
    # if str(real) == str(dic["expect"]):
    #     print("用例执行通过")
    # else:
    #     print("用例执行失败")
    #     raise AssertionError("预期结果与实际结果不否，断言失败")

    # 坑二：用try捕获异常把断言报错捕获走了，需要在except中重新抛出异常
    # try:
    #     assert str(real) == str(dic["expect"]),"预期结果与实际结果不否，断言失败"
    # except Exception as msg:
    #     print(msg)
    #     raise

    @pytest.mark.parametrize("dic", test_data)
    def test_case3(self, dic):
        ps = PreSolve(test_data)
        # 替换有依赖数据的值
        dic["header"], dic["value"] = ps.presolve(dic)

        # 请求接口获取响应结果
        rd = ConfigHttp(dic)
        res = rd.run()

        pa = PublicAssert(dic, res)
        pa.public_assert()


if __name__ == '__main__':
    pytest.main()
