# # 导入pytest
# import pytest, time
#
#
#
# # def test_case001():
# #     # time.sleep(3)
# #     print("\n执行测试用例001")
# #     #
# #     # 断言
# #     assert 1 == 1, "这两个数不相等"
#
#
# # 定义测试用例类
# class TestCase:
#     @pytest.mark.smoke
#     def test_case002(self):
#         # time.sleep(3)
#         print("\n执行测试用例002")
#
#         # 断言
#         assert 1 == 11, "这两个数不相等"
#
#     # @pytest.mark.skip("跳过这一条")
#     # @pytest.mark.skipif(1 == 11, reason="条件成立跳过，否则执行")
#
#     @pytest.fixture(autouse=True)
#     def test_case003(self):
#         print("\n我是用例执行之前的初始化工作，比如连接数据库打开文件等++++++++++++++++++++++++++++++++")
#         # time.sleep(3)
#         # 跳过测试用例
#         # pytest.skip("当前用例正在调试，暂时跳过")
#         print("\n执行测试用例003")
#
#         # 断言
#         assert 1 == 1, "这两个数不相等"
#
#     # @pytest.mark.run(order=2)
#     # def test_case004(self):
#     #     # time.sleep(3)
#     #     print("\n执行测试用例004")
#     #
#     #     # 断言
#     #     assert 1 == 1, "这两个数不相等"
#
#     # @pytest.mark.run(order=1)
#     # def test_case005(self):
#     #     # time.sleep(3)
#     #     print("\n执行测试用例005")
#     #
#     #     # 断言
#     #     assert 1 == 1, "这两个数不相等"
#
#     # @pytest.mark.g1
#     # def test_case007(self):
#     #     # time.sleep(3)
#     #     print("\n执行测试用例007")
#     #
#     #     # 断言
#     #     assert 6 == 7, "这两个数不相等"
# #
# #
# # if __name__ == '__main__':
# #     # 主函数模式
# #     pytest.main()
# #     # pytest.main(["-vs"]) #可以展示正常用例的结果
# #     # pytest.main(["-vs","-n=4"]) # -n=? 几个cpu同时执行
# #     # pytest.main(["-vs","--reruns=4"]) # 重试失败的用例
# #     pytest.main(["-vs", "--reruns=4", r"--html=D:\CeKai\framework_vip33\testReport\report.html"]) #生成原生的html测试报告
