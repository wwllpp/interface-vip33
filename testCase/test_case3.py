"""
导包
1、获取测试数据
2、定义一个测试类
    2.1 创建测试用例方法
        2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
        2.1.2 判断method的请求方式：
            如果是get：进行get请求并得到响应
            如果是post：进行post请求并得到响应
        2.1.3 将放回的json转化为字典
        2.1.4 在字典内获取实际的errorCode的值
        2.1.5 进行断言：判断实际结果与预期结果是否一致
"""
import pytest

#
# # 导包
# import pytest, requests
# from common.readData import ReadData
# import json
#
# # 1、获取测试数据
# re = ReadData()
# test_data = re.read_excel()
#
#
# # print(test_data[0])
#
# # 2、定义一个测试类
# class TestCase:
#     # 2.1 创建测试用例方法
#     def test_case(self):
#         # 2.1.1 获取测试数据内进行接口请求需要的关键字段：url,method,value,expect
#         url = test_data[0]["interfaceUrl"]
#         method = test_data[0]["method"]
#         value = test_data[0]["value"]
#         expect = test_data[0]["expect"]
#         print(url, method, value, expect)
#         # 2.1.2 判断method的请求方式：
#         # 如果是get：进行get请求并得到响应
#         if method.lower() == "get":
#             # 进行请求
#             res = requests.get(url=url,params=value)
#             # 查看结果
#             print(res)
#         # 如果是post：进行post请求并得到响应
#         elif method.lower() == "post":
#             res = requests.post(url=url,data=eval(value))
#             # print(res.text)
#             # 将字典直接给形参json
#             # res = requests.post(url=url,json=value)
#             # print(res.text)
#             # print(type(res.text))
#
#         # 2.1.3 将返回的json转化为字典
#         res_dict = res.json()
#         print(res_dict)
#         print(type(res_dict))
#         # 2.1.4 在字典内获取实际的errorCode的值
#         real = res_dict["errorCode"]
#         print(real)
#         # 2.1.5 进行断言：判断实际结果与预期结果是否一致
#         assert str(real) == str(expect),"预期结果与实际结果不否，断言失败"
#
#
# if __name__ == '__main__':
#     pytest.main()
#
# list1 = [1, 2, 3, 4, 5]
# list2 = [[1, 2], [2, 2], [3, 2], [4, 2]]
# list3 = [{"name": "小明", "age": 18}, {"name": "小红", "age": 20}]
#
#
# class TestApi:
#     # 单个值
#     # @pytest.mark.parametrize("args", list1)
#     # def test_case1(self, args):
#     #     print(f"\n当前执行的是第{args}条用例")
#     #
#     #     assert args == 2,"这个数不等于2"
#
#     # 列表套列表的取值，多个值
#     # @pytest.mark.parametrize("num1,num2",list2)
#     # def test_case2(self,num1,num2):
#     #     print(f"\n执行用例的参数一:{num1},执行用例的参数二:{num2}")
#     #
#     #     assert num1 == num2,"这两个数不相等"
#
#     # 列表里面套字典
#     @pytest.mark.parametrize("dict1", list3)
#     def test_case3(self, dict1):
#         print(f"\n现在执行的是{dict1}用例")
#
#         age = dict1["age"]
#         print(age)
#
#         assert age == 18, "这个学生年龄超出"
#
#
# if __name__ == '__main__':
#     pytest.main()
