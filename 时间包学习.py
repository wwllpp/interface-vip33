# 导包
import time

# # 获取时间戳 1970年1月1日早晨8点到现在多少秒
# print(time.time())
#
# # 获取当前的时间对象
# print(time.localtime())
#
# # 获取0 时区的时间对象
# print(time.gmtime())
#
# # 将时间对象转化成时间戳
# print(time.mktime(time.localtime()))
#
# # 睡眠
# # print(time.sleep(3))
#
# # 将时间对象转化为固定的格式输出
# print(time.asctime())
#
# # 将时间戳转化为固定的格式输出
# print(time.ctime())
#
# # 将时间对象转化为自定义格式输出
# print(time.strftime("%Y-%m-%d-%H-%M-%S"))
#
# # 将表示时间的字符串反转化为时间对象
# print(time.strptime("2024-05-19-11-13-24","%Y-%m-%d-%H-%M-%S"))

